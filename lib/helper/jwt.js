'use strict';

const BaseHelper = require('ecom-base-lib').BaseHelper;
const Joi = require('joi');
const IdentityJSONToken = require('ecom-base-lib').authz.IdentityJSONToken;
const AuthSchema = require('../schema/gateway-schema').AuthJWTToken;

const expiresInOneDay = (Date.now() / 1000) + 86400;

class Jwt extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.config = config;
  }

  base64Decode(base64EncodedCookie) {
    let buf = Buffer.from(base64EncodedCookie, 'base64');
    return buf.toString('');
  }

  createToken(credentials) {
    const me = this;
    let token = new IdentityJSONToken({
      id: credentials.id,
      email: credentials.email,
      permissions: credentials.permissions,
      exp: credentials.expiry || expiresInOneDay
    });
    const jwtToken = token.sign(me.config.jwt.privateKey.key, { algorithm: me.config.jwt.privateKey.algo });
    return new Buffer(jwtToken).toString('base64');
  }

  static validate(credentials) {
    return Joi.validate(credentials, AuthSchema);
  }

  static verify(token, publicKey) {
    try {
      let buf = Buffer.from(token, 'base64');
      let tokString = buf.toString('');
      return IdentityJSONToken.verify(tokString, publicKey);
    } catch (err) {
      return null;
    }
  }


}

module.exports = Jwt;
