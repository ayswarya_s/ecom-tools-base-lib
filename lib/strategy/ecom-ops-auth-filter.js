'use strict';

const Joi = require('joi');
const ExpressUserAgent = require('express-useragent');
const webJWTTokenCookie = 's_ecom_ops_jwt';
const constants = require('ecom-fabric-lib').Common.Constants;
const enums = require('ecom-fabric-lib').Common.Enum;
const JWTHelper = require('../helper/jwt');
const _ = require('lodash');
const Boom = require('boom');
const co = require('co');

const loginTypeSSO = 'sso-login';
// Declare internals

const internals = {};

exports.register = function (server, options, next) {
  server.auth.scheme('ecom-ops-auth', internals.implementation);
  next();
};

internals.schema = Joi.object({dependencies: Joi.any().required(), config: Joi.any().required()});

exports.register.attributes = {
  pkg: require('../../package.json')
};

internals.implementation = function (server, options) {
  /*eslint-disable no-inner-declarations*/
  try {

    function _getDeviceType (userAgent) {
      let deviceType = constants.DefaultDeviceType;
      _.forEach(enums.DeviceType, (type) => {
        if (userAgent[`is${type}`]) {
          deviceType = type;
          return false;
        }
      });
      return deviceType;
    }

    const scheme = {
      authenticate: function (request, reply) {
        return co(function * () {
          try {
            if (request.path.toLowerCase().includes('documentation') ||
              request.path.toLowerCase().includes('swagger.json') ||
              (request.headers[constants.RefererHeader] || '').toLowerCase().includes('documentation')) {
              return reply.continue({credentials: {}});
            }
            const authzToken = request.state[webJWTTokenCookie];
            const authType = request.state['x-ecom-ops-user-login'];

            let credentials;
            let ua = request.headers['user-agent'];
            let userAgent = ExpressUserAgent.parse(ua || '');

            let trueClientIP = request.headers['true-client-ip'];
            let clientIP = request.headers['client-ip'];
            let xForwardedFor = request.headers['x-forwarded-for'];
            let ip = trueClientIP || clientIP || xForwardedFor;
            if (!ip && request.info) {
              ip = request.info.remoteAddress || null;
            }
            options.dependencies.logger.log('info', { scope: 'ecom-ops-auth-filter', method: 'authenticate', message: 'client ip', 'true-client-ip': trueClientIP, 'client-ip': clientIP, 'x-forwarded-for': xForwardedFor});

            let clientIdentity = {
              ipAddress: ip,
              port: request.info ? request.info.remotePort : null,
              ua,
              detectedUA: userAgent.browser,
              os: userAgent.os,
              platform: userAgent.platform,
              deviceType: _getDeviceType(userAgent)
            };
            if (authzToken) {
              const token = JWTHelper.verify(authzToken, options.config.jwt.publicKey.key);
              const tokenValidation = JWTHelper.validate(token);

              if (tokenValidation.error) {
                options.dependencies.logger.log('error', {requestId: request.headers ? request.headers['x-request-id'] : null, type: 'ecom-ops-auth-filter JWT schema validation error', errorStack: {}, errorMsg: tokenValidation.error, state: request.state, headers: request.headers});
              }
              if (!token) {
                options.dependencies.logger.log('error', {requestId: request.headers ? request.headers['x-request-id'] : null, type: 'ecom-ops-auth-filter JWT token verification error', errorStack: {}, errorMsg: "Invalid JWT Token", state: request.state, headers: request.headers});
              }
              if (!token || tokenValidation.error) {
                if (authType == loginTypeSSO) {
                  return reply({error: 'Login to SSO'}).code(401);
                } else {
                  return reply({error: 'login to knox'}).code(401);
                }
              }
              credentials = {
                id: token.id,
                email: token.email,
                permissions: token.permissions
              };
              credentials.clientIdentity = clientIdentity;
              return reply.continue({credentials: JSON.stringify(credentials)});
            } else {
              return reply.redirect("/ops-web/login");
            }

          } catch(err) {
            options.dependencies.logger.log('error', {requestId: request.headers ? request.headers['x-request-id'] : null, type: 'ecom-auth-filter Authenticate Error', errorStack: err.stack, errorMsg: err.message, state: request.state, headers: request.headers});
            return reply(Boom.badRequest(err.message));
          }
        });
      }
    };
    return scheme;
  } catch(ex) {
    options.dependencies.logger.log('error', { type: 'ecom-ops-auth-filter Implementation error', errorStack: ex.stack, errorMsg: ex.message });
    throw ex;
  }
};
