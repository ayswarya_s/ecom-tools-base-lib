'use strict'

const BaseServiceClient = require('ecom-base-lib').BaseServiceClient,
  co = require('co');


class SSOApiClient extends BaseServiceClient {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
  }

  validateUser(token) {
    const me = this;
    return co(function* () {
      try {
        let result = yield me._postMultiPartFormData('http://samsunglocal.samsung.com:8111/us/api/sup/tgt/validatecdssotkn', {}, {'token': token});
        return result;
      } catch (error) {
        throw error;
      }
    });
  }

  login(payload) {
    const me = this;
    return co(function* () {
      try {
        let result = yield me._postMultiPartFormData('http://samsunglocal.samsung.com:8111/us/api/sup/sso/signin', {}, payload);
        return result;
      } catch(error) {
        throw error;
      }
    });
  }
}
module.exports = SSOApiClient
