'use strict'

const BaseServiceClient = require('ecom-base-lib').BaseServiceClient,
  co = require('co');


class PermissionsClient extends BaseServiceClient {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
  }

  getUserPermissions(email) {
    const me = this;
    return co(function* () {
      try {
        let result = yield me._get('http://localhost:1980/v1/users?email_id='+email);
        return result;
      } catch (error) {
        throw error;
      }
    });
  }
}
module.exports = PermissionsClient
