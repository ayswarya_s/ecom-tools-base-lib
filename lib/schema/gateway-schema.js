'use strict';
const joi = require('joi');

const AuthJWTToken = joi.object({
  schema_version: joi.string().required(),
  exp: joi.number().required(),
  iat: joi.number().integer().required(),
  id: joi.string().description('Unique user id').required(),
  email: joi.string().email().description("User login email").required(),
  permissions: joi.object().required()
});

module.exports = {
  AuthJWTToken
}
