#ECOM Tools Base lib

This node module serves as a library for managing schema, strategies and clients used across tools services.

## Setup
`npm install`

## Debug
1. `npm install -g node-inspector`
2. Run `node-inspector` in terminal
3. Run `node --debug-brk server.js` in another terminal tab
4. Launch http://localhost:8080?port=5858 to start debugging

## Run tests
`./node_modules/.bin/mocha tests/**/*.js`