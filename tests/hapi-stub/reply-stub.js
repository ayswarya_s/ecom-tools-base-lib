"use strict";

class ReplyStub {
  constructor(dependencies) {
    this.dependencies = dependencies;
    this.promise = new Promise((resolve, reject)=> this.resolveFunc(resolve, reject));
    this.reply = (response) => this.response(response);
    this.reply.code = (statusCode) => this.code(statusCode);
    this.reply.continue = (data) => this.continue(data);
    this.reply.state = (name, value) => this.state(name, value);
    this.reply.redirect = (url) => this.redirect(url);
    this.cookies = {};
    this.continued = false;
  }

  log(message) {
    this.dependencies.logger.log(message);
  }

  resolveFunc(resolve, reject) {
    this.resolve = resolve;
    this.reject = reject;
  }

  response(response) {
    const me = this;
    this.log('Responding to the reply stub...')
    this._response = response;
    setTimeout(() => {
      me.resolve(me.reply);
    }, 50);

    return this.reply;
  }

  code(code) {
    this._code = code;
    return this;
  }

  state(name, value) {
    this.cookies[name] = value;
  }

  continue(data) {
    const me = this;
    this.continued = true;
    this.data = data;
    setTimeout(() => {
      me.resolve(me.reply);
    }, 50);
    return;
  }

  redirect(url) {
    const me = this;
    this.redirected = true;
    this.url = url;
    setTimeout(() => {
      me.resolve(me.reply)
    }, 50);
    return;
  }
}

module.exports = ReplyStub;
