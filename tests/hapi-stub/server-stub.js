"use strict";

class AuthSchemeStub {
  constructor() {
  }

  scheme(scheme, implementation) {
    this.scheme = scheme;
    this.implementation = implementation;
  }
}

class ServerStub {
  constructor() {
    this.auth = new AuthSchemeStub();
  }
  ext(stage, callback) {
    this.stage = stage;
    this.callback = callback;
  }

  route(options) {
    this.handlerOptions = options;
  }

  log(data) {
    console.log(data);
  }
}

module.exports = ServerStub;
