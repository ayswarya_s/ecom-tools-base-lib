"use strict";

const config = require('../../config/config'),
  assert = require('assert'),
  ReplyStub = require('../hapi-stub/reply-stub'),
  ServerStub = require('../hapi-stub/server-stub'),
  EComAuthFilter = require('../../lib/strategy/ecom-ops-auth-filter'),
  IdentityJSONToken = require('ecom-base-lib').authz.IdentityJSONToken,
  JWTHelper = require('../../lib/helper/jwt'),
  fs = require('fs'),
  path = require('path'),
  sinon = require('sinon'),
  _ = require('lodash'),
  co = require('co');

let options = {};
let dependencies = {logger: console};

describe('EComOpsAuthFilter', function(){

  before(function(done) {
    options.dependencies = dependencies;
    options.config = config;

    config.jwt = {};
    let publicKey = fs.readFileSync(path.join(__dirname, '../keys/public-key.pem')),
      privateKey = fs.readFileSync(path.join(__dirname, '../keys/private-key.pem'));
    config["jwt"]["publicKey"] = {"key": publicKey, "algo": "RS256"};
    config["jwt"]["privateKey"] = {"key": privateKey, "algo": "RS256"};

    options.dependencies = dependencies;
    options.config = config;
    done();
  });

  function createWebJWTToken(dependencies, config) {
    const jwtHelper = new JWTHelper(dependencies, config);
    let finalEncodedToken = jwtHelper.createToken({
      id: '71052efb-03a0-43cd-b737-bbd98127abcd',
      email: 'opsuser@samsung.com',
      permissions: {
        order: {
          allow: 'read'
        }
      }
    });
    return {
      query: {},
      payload: null,
      headers: {},
      state: {
        's_ecom_ops_jwt': finalEncodedToken,
        'x-ecom-ops-user-login': 'sso-login'
      },
      path: '/api/v1/blah'
    };
  }

  function createInvalidTokenKnox(dependencies, config) {
    return {
      query: {},
      payload: null,
      headers: {},
      state: {
        's_ecom_ops_jwt': 'invalidtoken',
        'x-ecom-ops-user-login': 'knox-login'
      },
      path: '/api/v1/blah'
    };
  }

  function createInvalidTokenSSO(dependencies, config) {
    const jwtHelper = new JWTHelper(dependencies, config);
    return {
      query: {},
      payload: null,
      headers: {},
      state: {
        's_ecom_ops_jwt': 'invalidtoken',
        'x-ecom-ops-user-login': 'sso-login'
      },
      path: '/api/v1/blah'
    };
  }

  function createInvalidLoginType(dependencies, config) {
    const jwtHelper = new JWTHelper(dependencies, config);
    let finalEncodedToken = jwtHelper.createToken({
      id: '71052efb-03a0-43cd-b737-bbd98127abcd',
      email: 'opsuser@samsung.com',
      permissions: {
        order: {
          allow: 'read'
        }
      }
    });
    return {
      query: {},
      payload: null,
      headers: {},
      state: {
        's_ecom_ops_jwt': finalEncodedToken,
        'x-ecom-ops-user-login': 'invalid-login-type'
      },
      path: '/api/v1/blah'
    };
  }

  function createRequestWithNoToken(dependencies, config) {
    return {
      query: {},
      payload: null,
      headers: {},
      state: {},
      path: '/api/v1/blah'
    };
  }

  function createDocumentationRequest() {
    return {
      query: {},
      headers: {
        referrer: 'test'
      },
      payload: null,
      state: {},
      params: {},
      path: 'documentation.json'
    };
  }

  function createSwaggerRequest() {
    return {
      query: {},
      headers: {
        referrer: 'test'
      },
      payload: null,
      state: {},
      params: {},
      path: 'swagger.json'
    };
  }

  it ('skips authentication for documentation url', function (done) {
    const request = createDocumentationRequest();
    const replyStub = new ReplyStub(dependencies);
    const serverStub = new ServerStub();
    EComAuthFilter.register(serverStub, options, function() {
      let scheme = serverStub.auth.implementation(serverStub, options);
      return co(function*() {
        yield scheme.authenticate(request, replyStub.reply);
        replyStub.promise.then(function(result) {
          assert.equal(replyStub.continued, true);
          assert.deepEqual(replyStub.data.credentials, {});
          done();
        });
      });
    });
  });


  it ('skips authentication for swagger url', function (done) {
    const request = createSwaggerRequest();
    const replyStub = new ReplyStub(dependencies);
    const serverStub = new ServerStub();
    EComAuthFilter.register(serverStub, options, function() {
      let scheme = serverStub.auth.implementation(serverStub, options);
      return co(function*() {
        yield scheme.authenticate(request, replyStub.reply);
        replyStub.promise.then(function(result) {
          assert.equal(replyStub.continued, true);
          assert.deepEqual(replyStub.data.credentials, {});
          done();
        });
      });
    });
  });

  it('when there is no token, redirects', function(done) {
    const request = createRequestWithNoToken(options.dependencies, options.config);
    const replyStub = new ReplyStub(dependencies), serverStub = new ServerStub();
    EComAuthFilter.register(serverStub, options, function() {
      let scheme = serverStub.auth.implementation(serverStub, options);
      return co(function*() {
        yield scheme.authenticate(request, replyStub.reply);
        replyStub.promise.then(function(result) {
          assert.equal(replyStub.redirected, true);
          assert.equal(replyStub.url, '/ops-web/login');
          done();
        }, function(err) {
          assert.equal(null, err);
          done();
        });
      });
    });
  });

  it('when token is invalid and login is knox, throws knox login error', function(done) {
    const request = createInvalidTokenKnox(dependencies, config);
    const replyStub = new ReplyStub(dependencies), serverStub = new ServerStub();
    EComAuthFilter.register(serverStub, options, function() {
      let scheme = serverStub.auth.implementation(serverStub, options);
      return co(function*() {
        yield scheme.authenticate(request, replyStub.reply);
        replyStub.promise.then(function(result) {
          assert.equal(replyStub._response.error, "login to knox");
          assert.equal(replyStub._code, 401);
          done();
        }, function(err) {
          assert.equal(null, err);
          done();
        });
      });
    });
  });

  it('when token is invalid and login is sso, throws sso login error', function(done) {
    const request = createInvalidTokenSSO(dependencies, config);
    const replyStub = new ReplyStub(dependencies), serverStub = new ServerStub();
    EComAuthFilter.register(serverStub, options, function() {
      let scheme = serverStub.auth.implementation(serverStub, options);
      return co(function*() {
        yield scheme.authenticate(request, replyStub.reply);
        replyStub.promise.then(function(result) {
          assert.equal(replyStub._code, 401);
          done();
        }, function(err) {
          assert.equal(null, err);
          done();
        });
      });
    });
  });

  it ('validate jwt token in cookie', function (done) {
    const request = createWebJWTToken(options.dependencies, options.config);
    const replyStub = new ReplyStub(dependencies);
    const serverStub = new ServerStub();
    EComAuthFilter.register(serverStub, options, function() {
      let scheme = serverStub.auth.implementation(serverStub, options);
      return co(function*() {
        yield scheme.authenticate(request, replyStub.reply);
        replyStub.promise.then(function(result) {
          assert.equal(replyStub.continued, true);
          let jsonResponse = JSON.parse(replyStub.data.credentials);
          assert.equal(jsonResponse.email, 'opsuser@samsung.com');
          assert.equal(jsonResponse.permissions.order.allow, 'read');
          done();
        });
      })
    })
  });

});
