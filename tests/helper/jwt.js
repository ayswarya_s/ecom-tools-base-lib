"use strict";

const JWTHelper = require('../../lib/helper/jwt');
const config = require('../../config/config');
const assert = require('assert');
const sinon = require('sinon');
const dependencies = { logger: console };
const co = require('co');
const fs = require('fs');
const path = require('path');

before((done) => {
  config.jwt = {};
  let publicKey = fs.readFileSync(path.join(__dirname, '../keys/public-key.pem')),
    privateKey = fs.readFileSync(path.join(__dirname, '../keys/private-key.pem'));
  config["jwt"]["publicKey"] = {"key": publicKey, "algo": "RS256"};
  config["jwt"]["privateKey"] = {"key": privateKey, "algo": "RS256"};

  config.appId = 'ecom-tools-base-lib';
  done();
});

describe('JWTHelper', () => {
  it ('sets config', (done) => {
    let jwtHelper = new JWTHelper(dependencies, config);

    assert.equal(jwtHelper.config, config);
    done();
  });

  describe("#createToken", () => {
    it('signs the data with private key', (done) => {
      let jwtHelper = new JWTHelper(dependencies, config);
      let credentials = jwtHelper.createToken({
        id: 'uuid',
        email: 'opsuser@samsung.com',
        permissions: {}
      });


      let encodedToken = jwtHelper.createToken(credentials);

      assert.notEqual(encodedToken, "");
      assert.notEqual(encodedToken, null);
      assert.notEqual(encodedToken, undefined);
      done();
    });
  });

  describe('#verify', () => {
    it('throws error if invalid schema', (done) => {
      let credentials = {
        key_1: 'value',
        key_2: 'value'
      }
      let tokenValidate = JWTHelper.validate(credentials);

      assert.notEqual(tokenValidate.error, null);
      done();
    });

    it('validates sucessfully for valid schema', (done) => {
      let credentials = {
        id: 'uuid',
        email: 'opsuser@samsung.com',
        iat: 1,
        exp: 1,
        permissions: {},
        schema_version: "1"
      }
      let tokenValidate = JWTHelper.validate(credentials);

      assert.equal(tokenValidate.error, null);
      done();

    });
  });

});
